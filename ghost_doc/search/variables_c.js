var searchData=
[
  ['n',['n',['../structghost__gemm__perf__args.html#a47b0219c155fafa5dfe2c6dec3af70c2',1,'ghost_gemm_perf_args']]],
  ['name',['name',['../structghost__sparsemat.html#a1dbd077643c390b9117ebe003e132172',1,'ghost_sparsemat']]],
  ['names',['names',['../structghost__gpu__info.html#a8c62f01251e8397337440b48298c6a35',1,'ghost_gpu_info']]],
  ['nblock',['nblock',['../structghost__densemat.html#aa5a40f2f7872c8a8020ac2fa083752fc',1,'ghost_densemat']]],
  ['ncalls',['nCalls',['../structghost__timing__region.html#ac7422f7eaa2c5beb5505e2b969c87ad4',1,'ghost_timing_region']]],
  ['nchunkavg',['nChunkAvg',['../structghost__context.html#a4a672bb7fcc485afd0601cf1f2e3e0e7',1,'ghost_context']]],
  ['nchunks',['nchunks',['../structghost__sparsemat.html#a056276e4e9e24fb9a24facb3a496ec87',1,'ghost_sparsemat']]],
  ['ncolors',['ncolors',['../structghost__context.html#a4f95d59cbe7d2ea048a3ab33857a5d80',1,'ghost_context']]],
  ['ncols',['ncols',['../structghost__bincrs__header__t.html#a8ec0624cfd9064e7ae0c53304f0ef144',1,'ghost_bincrs_header_t::ncols()'],['../structghost__densemat__traits.html#a55eab0d20400a00ec504f4972f503592',1,'ghost_densemat_traits::ncols()'],['../structghost__axpy__perf__args.html#a00b8d38fe5633ba837f67abb267d6f27',1,'ghost_axpy_perf_args::ncols()'],['../structghost__axpby__perf__args.html#a7b50d9727d5273c35e3d2a3721295de0',1,'ghost_axpby_perf_args::ncols()'],['../structghost__axpbypcz__perf__args.html#a85bdb5532a2dc895f8bdb5f4e2affcb2',1,'ghost_axpbypcz_perf_args::ncols()'],['../structghost__dot__perf__args.html#a23e448763697420ba1920d6bb8bad03a',1,'ghost_dot_perf_args::ncols()'],['../structghost__scale__perf__args.html#a05883268a57adf45151861b05a02e649',1,'ghost_scale_perf_args::ncols()']]],
  ['ncolsin',['ncolsin',['../structghost__tsmm__inplace__parameters.html#a363ef5fa5c7b02d98adfc0ced49e819d',1,'ghost_tsmm_inplace_parameters']]],
  ['ncolsout',['ncolsout',['../structghost__tsmm__inplace__parameters.html#a9b27237c5ae1791702ec0a3c7fcf7434',1,'ghost_tsmm_inplace_parameters']]],
  ['ncolspadded',['ncolspadded',['../structghost__densemat__traits.html#a49ff6ab97bab17ab5ba32738496296c9',1,'ghost_densemat_traits']]],
  ['ncolssub',['ncolssub',['../structghost__densemat__traits.html#a209825de30cb92696ec850b24a9b8662',1,'ghost_densemat_traits']]],
  ['ncore',['ncore',['../structghost__hwconfig.html#a2fb49c6c350198f289eed197460aa32b',1,'ghost_hwconfig']]],
  ['ndepends',['ndepends',['../structghost__task.html#a4f0c35255eb30b44e27fe33762ee2c8d',1,'ghost_task']]],
  ['ndevice',['ndevice',['../structghost__gpu__info.html#aaac5b7e3760ddd8ca717150b57126198',1,'ghost_gpu_info']]],
  ['ndistinctdevice',['ndistinctdevice',['../structghost__gpu__info.html#ac2d8de19d3101b691d8f5cef30254235',1,'ghost_gpu_info']]],
  ['ndomains',['nDomains',['../structghost__pumap.html#a6ec06f6a5824167c91aa79f2d33d7a3e',1,'ghost_pumap']]],
  ['nduepartners',['nduepartners',['../structghost__context.html#a0cfee08dfe98d5aff6f6730fb8d09f7f',1,'ghost_context']]],
  ['nelemavg',['nElemAvg',['../structghost__context.html#ad2607e984f986a968aa284a0d6696c14',1,'ghost_context']]],
  ['nents',['nEnts',['../structghost__sparsemat.html#aa2304176deb186ecce4ef908e6bf515f',1,'ghost_sparsemat']]],
  ['nentsinrow',['nEntsInRow',['../structghost__sorting__helper.html#abda7adf8ed0f5a7c8b970032eb892b60',1,'ghost_sorting_helper']]],
  ['newtaskcond_5fby_5fthreadcount',['newTaskCond_by_threadcount',['../taskq_8c.html#a4046feadb5dd3f5862ebbee3b6ebf271',1,'taskq.c']]],
  ['newtaskmutex_5fby_5fthreadcount',['newTaskMutex_by_threadcount',['../taskq_8c.html#a2bb9e68a85065dd3465df69658f2a521',1,'taskq.c']]],
  ['next',['next',['../structghost__task.html#a968fb2a68a7d0331fd8c3a34d6e56b3b',1,'ghost_task']]],
  ['nhalo',['nhalo',['../structghost__map.html#a504bd522ae8588a7592027b296788c5c',1,'ghost_map']]],
  ['nmats',['nmats',['../structghost__context.html#afe73b225964883ac993b582371e642c7',1,'ghost_context']]],
  ['nmaxrows',['nMaxRows',['../structghost__sparsemat.html#ad5982b0180fd792f139c502d89422ccc',1,'ghost_sparsemat']]],
  ['nnz',['nnz',['../structghost__bincrs__header__t.html#af7be31e7227e57e44bb76255d8f4d653',1,'ghost_bincrs_header_t::nnz()'],['../structghost__context.html#a1efe958b3d600a926e9187a969623949',1,'ghost_context::nnz()']]],
  ['normalize',['normalize',['../structghost__kacz__opts.html#afbde47028b8a4d114d56b71255fc466f',1,'ghost_kacz_opts::normalize()'],['../structghost__carp__opts.html#a8b4925bf63fdaed4305c7a0af553b428',1,'ghost_carp_opts::normalize()']]],
  ['nrand',['nrand',['../rand_8c.html#a73d3e828afff388d74eb20ae5b236f00',1,'rand.c']]],
  ['nrankspresent',['nrankspresent',['../structghost__context.html#a8b83897e75cd8df2f585630394642d91',1,'ghost_context']]],
  ['nrows',['nrows',['../structghost__bincrs__header__t.html#a31961857a17d192c971b5b6ba382bba9',1,'ghost_bincrs_header_t']]],
  ['nshifts',['nshifts',['../structghost__kacz__parameters.html#a20fb8b2d26b8132efa90e3579415fe2f',1,'ghost_kacz_parameters']]],
  ['nsmt',['nsmt',['../structghost__hwconfig.html#a1dc99a0c7308030ca0898cb113042d02',1,'ghost_hwconfig']]],
  ['nsub',['nsub',['../structghost__densemat.html#a4f39c4339ce8556245c03e2f4154d449',1,'ghost_densemat']]],
  ['nthreadcount',['nthreadcount',['../taskq_8c.html#a2898af1a464d5cb3ca6a8e2a84016ad3',1,'taskq.c']]],
  ['nthreads',['nThreads',['../structghost__task.html#a9c3cb44b93b59f95660637ba47a4e39d',1,'ghost_task::nThreads()'],['../structghost__thpool.html#afb8871a6a64c760daad4cc84de4be801',1,'ghost_thpool::nThreads()']]],
  ['num_5fpending_5ftasks',['num_pending_tasks',['../taskq_8c.html#a1e362ed45023b6b0c799c82f4af7c79f',1,'taskq.c']]],
  ['num_5fshep_5fby_5fthreadcount',['num_shep_by_threadcount',['../taskq_8c.html#aca979a25d725431b7097a133f3bda45e',1,'taskq.c']]],
  ['num_5fshifts',['num_shifts',['../structghost__kacz__opts.html#aa7b52468dab76acc2cdb03a46b8f4feb',1,'ghost_kacz_opts::num_shifts()'],['../structghost__carp__opts.html#a6edc49d754eebbc7d600c7a7919e9447',1,'ghost_carp_opts::num_shifts()']]],
  ['num_5ftasks_5fby_5fthreadcount',['num_tasks_by_threadcount',['../taskq_8c.html#a7116266bc8ad2565ba9aa23485afd5e0',1,'taskq.c']]],
  ['nwishpartners',['nwishpartners',['../structghost__context.html#ad5f40ba43ebfc6e3f717ef4ffa5114b9',1,'ghost_context']]],
  ['nzdist',['nzDist',['../structghost__sparsemat.html#a4a03940f852c3eaba721c24ab698af2c',1,'ghost_sparsemat']]],
  ['nzones',['nzones',['../structghost__context.html#a47acc1a323d3b17edc5649e2eef90e3f',1,'ghost_context']]]
];
