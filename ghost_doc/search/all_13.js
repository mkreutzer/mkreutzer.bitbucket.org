var searchData=
[
  ['unroll',['unroll',['../structghost__tsmm__parameters.html#abb245a29a8f9a05b74ed769792b28102',1,'ghost_tsmm_parameters::unroll()'],['../structghost__tsmttsm__parameters.html#a7a9f8de5f013f2d2e90954aabfdc5db7',1,'ghost_tsmttsm_parameters::unroll()']]],
  ['unused',['UNUSED',['../util_8h.html#a86d500a34c624c2cae56bc25a31b12f3',1,'util.h']]],
  ['upperbandwidth',['upperBandwidth',['../structghost__context.html#abceb094dbc9daee293f752530847a593',1,'ghost_context']]],
  ['upperperc90dists',['upperPerc90Dists',['../sparsemat_8cpp.html#a64e505dce2ff0b9a4294c2b6f8a5a6cc',1,'sparsemat.cpp']]],
  ['used',['used',['../structSmallBuffer.html#a299967a04b6ef1c745b822a4ca7b6c18',1,'SmallBuffer']]],
  ['util_2ec',['util.c',['../util_8c.html',1,'']]],
  ['util_2eh',['util.h',['../util_8h.html',1,'']]]
];
