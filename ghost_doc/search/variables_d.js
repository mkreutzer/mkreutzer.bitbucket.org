var searchData=
[
  ['offs',['offs',['../structghost__map.html#a96f6717f85dfe685138356f22106d239',1,'ghost_map::offs()'],['../structghost__sparsemat__rowfunc__crs__arg.html#a43af4f170c74a162691a734e135d32b1',1,'ghost_sparsemat_rowfunc_crs_arg::offs()']]],
  ['omega',['omega',['../structghost__kacz__opts.html#a3250e4b6a5528f96fb2de95d97cde119',1,'ghost_kacz_opts::omega()'],['../structghost__carp__opts.html#a9440e84ad001789881dad9626419af26',1,'ghost_carp_opts::omega()']]],
  ['opt_5fblockvec_5fwidth',['opt_blockvec_width',['../structghost__sparsemat__traits.html#a48c4caaa0c7d3b1ac3404911af60cb3e',1,'ghost_sparsemat_traits']]],
  ['out',['OUT',['../structghost__compatible__vec__vec.html#a79a9b19bd3208ff2c4287de3df1fa3f0',1,'ghost_compatible_vec_vec']]],
  ['out_5fa',['OUT_A',['../structghost__compatible__vec__init.html#ae8f2cad5a53dfc5be1314c0a149ee45b',1,'ghost_compatible_vec_init']]],
  ['out_5fb',['OUT_B',['../structghost__compatible__vec__init.html#a1062a107769e2ee0f5fc4b501ce771f6',1,'ghost_compatible_vec_init']]]
];
