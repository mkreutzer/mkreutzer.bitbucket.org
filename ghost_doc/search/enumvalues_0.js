var searchData=
[
  ['ghost_5faligned',['GHOST_ALIGNED',['../types_8h.html#ac2562a957b35f810f4f24f2eeb9448faafe9d53f941d6c1e08a0058de97afc86b',1,'types.h']]],
  ['ghost_5fbench_5fload',['GHOST_BENCH_LOAD',['../bench_8h.html#af72313545dc41e91b82c375fcf0f6d49aaad02f201ea76cd85eecbf0cec9b735f',1,'bench.h']]],
  ['ghost_5fbench_5fstore',['GHOST_BENCH_STORE',['../bench_8h.html#af72313545dc41e91b82c375fcf0f6d49a70f8ffd6380b14469d817f757537a0da',1,'bench.h']]],
  ['ghost_5fbench_5fstream_5fcopy',['GHOST_BENCH_STREAM_COPY',['../bench_8h.html#af72313545dc41e91b82c375fcf0f6d49ae2f12d34732e5ff31b4717515e636222',1,'bench.h']]],
  ['ghost_5fbench_5fstream_5ftriad',['GHOST_BENCH_STREAM_TRIAD',['../bench_8h.html#af72313545dc41e91b82c375fcf0f6d49afe68a9d83e2d19e86d6353e5cec0d5ee',1,'bench.h']]],
  ['ghost_5fbench_5fupdate',['GHOST_BENCH_UPDATE',['../bench_8h.html#af72313545dc41e91b82c375fcf0f6d49af9c4ffb7c1065b78885935ae460ccbe9',1,'bench.h']]],
  ['ghost_5fcontext_5fdefault',['GHOST_CONTEXT_DEFAULT',['../context_8h.html#acfbc265c00c25e6e70a0d92e0c8629f8aae8b6795f8b4522a1c2dd4c51764ccd5',1,'context.h']]],
  ['ghost_5fcontext_5fdist_5fnz',['GHOST_CONTEXT_DIST_NZ',['../context_8h.html#acfbc265c00c25e6e70a0d92e0c8629f8a303fb3406c9df50b745851abe94101ed',1,'context.h']]],
  ['ghost_5fcontext_5fdist_5frows',['GHOST_CONTEXT_DIST_ROWS',['../context_8h.html#acfbc265c00c25e6e70a0d92e0c8629f8ac13553d150b971f8446e4dedf81eb3a1',1,'context.h']]],
  ['ghost_5fdatatransfer_5fany',['GHOST_DATATRANSFER_ANY',['../datatransfers_8h.html#aa589eef505842634731ccab8cd3ae40da7d69176d8cdd836ca1da27ffba1bc041',1,'datatransfers.h']]],
  ['ghost_5fdatatransfer_5fin',['GHOST_DATATRANSFER_IN',['../datatransfers_8h.html#aa589eef505842634731ccab8cd3ae40da5be13d1da61ba5743d820d83b0b0e048',1,'datatransfers.h']]],
  ['ghost_5fdatatransfer_5fout',['GHOST_DATATRANSFER_OUT',['../datatransfers_8h.html#aa589eef505842634731ccab8cd3ae40da92653c8de2611cb9db6e8ad8b6f27a8c',1,'datatransfers.h']]],
  ['ghost_5fdensemat_5fcolmajor',['GHOST_DENSEMAT_COLMAJOR',['../densemat_8h.html#a676e25d1ba7b16029044c0a025f82c63a2ce3a33a3d6d3a2564baaa6177b81dc1',1,'densemat.h']]],
  ['ghost_5fdensemat_5fdefault',['GHOST_DENSEMAT_DEFAULT',['../densemat_8h.html#a1339073a9df80abe60795083cc4c15caafe91befc04943e9081383734f8480efa',1,'densemat.h']]],
  ['ghost_5fdensemat_5fnot_5frelocate',['GHOST_DENSEMAT_NOT_RELOCATE',['../densemat_8h.html#a1339073a9df80abe60795083cc4c15caa2ccd66433e8e88b74badef7ddf024446',1,'densemat.h']]],
  ['ghost_5fdensemat_5fpad_5fcols',['GHOST_DENSEMAT_PAD_COLS',['../densemat_8h.html#a1339073a9df80abe60795083cc4c15caa82257acb639b3324628d6d6dadd5b4f2',1,'densemat.h']]],
  ['ghost_5fdensemat_5fpermuted',['GHOST_DENSEMAT_PERMUTED',['../densemat_8h.html#a1339073a9df80abe60795083cc4c15caad52d327268b2bc41f8a3506624d98f71',1,'densemat.h']]],
  ['ghost_5fdensemat_5frowmajor',['GHOST_DENSEMAT_ROWMAJOR',['../densemat_8h.html#a676e25d1ba7b16029044c0a025f82c63afa460aed5848d75177265ba9411a71a4',1,'densemat.h']]],
  ['ghost_5fdensemat_5fscattered_5fld',['GHOST_DENSEMAT_SCATTERED_LD',['../densemat_8h.html#a1339073a9df80abe60795083cc4c15caa27f289c4f456dea90c1186e060a4ae84',1,'densemat.h']]],
  ['ghost_5fdensemat_5fscattered_5ftr',['GHOST_DENSEMAT_SCATTERED_TR',['../densemat_8h.html#a1339073a9df80abe60795083cc4c15caa106851fd562794253e0b50c96ef70134',1,'densemat.h']]],
  ['ghost_5fdensemat_5fstorage_5fdefault',['GHOST_DENSEMAT_STORAGE_DEFAULT',['../densemat_8h.html#a676e25d1ba7b16029044c0a025f82c63ad31617ce59d0cc44bf74924c1fdcc045',1,'densemat.h']]],
  ['ghost_5fdensemat_5fview',['GHOST_DENSEMAT_VIEW',['../densemat_8h.html#a1339073a9df80abe60795083cc4c15caa49c79195f48a8061dbd4b1f348abb602',1,'densemat.h']]],
  ['ghost_5fdt_5fc_5fidx',['GHOST_DT_C_IDX',['../types_8h.html#aee29881a214f0dc415f76027738bdd39a38781c7714dea73f809b270a71d628fb',1,'types.h']]],
  ['ghost_5fdt_5fcomplex',['GHOST_DT_COMPLEX',['../types_8h.html#af01e1c288cf0abf3d5c2a99ca62244d6a3897b9c2249968a706a09c4a6c4a5322',1,'types.h']]],
  ['ghost_5fdt_5fd_5fidx',['GHOST_DT_D_IDX',['../types_8h.html#aee29881a214f0dc415f76027738bdd39a7876da24570db94082759efc4e302e7a',1,'types.h']]],
  ['ghost_5fdt_5fdouble',['GHOST_DT_DOUBLE',['../types_8h.html#af01e1c288cf0abf3d5c2a99ca62244d6af5fc31a15a5830cd480598c0b3da4c17',1,'types.h']]],
  ['ghost_5fdt_5ffloat',['GHOST_DT_FLOAT',['../types_8h.html#af01e1c288cf0abf3d5c2a99ca62244d6ad8fba8ff7f11ca4813f6fcf52be5d295',1,'types.h']]],
  ['ghost_5fdt_5freal',['GHOST_DT_REAL',['../types_8h.html#af01e1c288cf0abf3d5c2a99ca62244d6a028244deb3f198d40d088af9b90dc812',1,'types.h']]],
  ['ghost_5fdt_5fs_5fidx',['GHOST_DT_S_IDX',['../types_8h.html#aee29881a214f0dc415f76027738bdd39a995ff3beb08e77439258cea97d1998b9',1,'types.h']]],
  ['ghost_5fdt_5fz_5fidx',['GHOST_DT_Z_IDX',['../types_8h.html#aee29881a214f0dc415f76027738bdd39aee025ca7729327890f35e157bf8208ed',1,'types.h']]],
  ['ghost_5ferr_5fblas',['GHOST_ERR_BLAS',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa0e9e01e5e86c9fe9ff2582989721a074',1,'error.h']]],
  ['ghost_5ferr_5fblockcolor',['GHOST_ERR_BLOCKCOLOR',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa6fbc82abe298460002a3ae8a89a32d0b',1,'error.h']]],
  ['ghost_5ferr_5fcolpack',['GHOST_ERR_COLPACK',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfac08065a991b7cd91f8897af3ed3a6a32',1,'error.h']]],
  ['ghost_5ferr_5fcompatibility',['GHOST_ERR_COMPATIBILITY',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa44deda250eb04a6da9ccfb726947d535',1,'error.h']]],
  ['ghost_5ferr_5fcublas',['GHOST_ERR_CUBLAS',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfacfbf21188f7492c6c4c0b00122d751eb',1,'error.h']]],
  ['ghost_5ferr_5fcuda',['GHOST_ERR_CUDA',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa8c4f2af330ff9e7d94c504aa580db1f2',1,'error.h']]],
  ['ghost_5ferr_5fcurand',['GHOST_ERR_CURAND',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa82b5313c206f7063203ffa85afcb80fd',1,'error.h']]],
  ['ghost_5ferr_5fcusparse',['GHOST_ERR_CUSPARSE',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfabb3e0fc376abba8de15649ae88132dfd',1,'error.h']]],
  ['ghost_5ferr_5fdatatype',['GHOST_ERR_DATATYPE',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa4d84e1aee7b70760d808d84a60a82a61',1,'error.h']]],
  ['ghost_5ferr_5fhwloc',['GHOST_ERR_HWLOC',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa626476f06ec6bf3931486bfc9129cb15',1,'error.h']]],
  ['ghost_5ferr_5finvalid_5farg',['GHOST_ERR_INVALID_ARG',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa3ed4af899620cd2c217dd90193ba37a5',1,'error.h']]],
  ['ghost_5ferr_5fio',['GHOST_ERR_IO',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa32ee9ac73731f95fa72ed69389bf580a',1,'error.h']]],
  ['ghost_5ferr_5flapack',['GHOST_ERR_LAPACK',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa8b5fbcce3ba567544f554cfd62fe55fa',1,'error.h']]],
  ['ghost_5ferr_5fmpi',['GHOST_ERR_MPI',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfaae9676b5ec008f74bbc30b5f07f3fbaa',1,'error.h']]],
  ['ghost_5ferr_5fnot_5fcolored',['GHOST_ERR_NOT_COLORED',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa07e6dff2494eb92db0f4f8d8525b2d5c',1,'error.h']]],
  ['ghost_5ferr_5fnot_5fimplemented',['GHOST_ERR_NOT_IMPLEMENTED',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa264896bc237454dec0cd04b92c2abaf6',1,'error.h']]],
  ['ghost_5ferr_5fred_5fblack',['GHOST_ERR_RED_BLACK',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfae3ac6af29ca313dc38425831affbc8db',1,'error.h']]],
  ['ghost_5ferr_5fscotch',['GHOST_ERR_SCOTCH',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfabfc2329eec2ed29bc5c3ef5f425093b0',1,'error.h']]],
  ['ghost_5ferr_5funknown',['GHOST_ERR_UNKNOWN',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa21238f11b077ec8a3abe371a018bde76',1,'error.h']]],
  ['ghost_5ferr_5fzoltan',['GHOST_ERR_ZOLTAN',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfaacbcb053f6d5871aac9671a857ec32c7',1,'error.h']]],
  ['ghost_5fgemm_5fdefault',['GHOST_GEMM_DEFAULT',['../math_8h.html#a5a51022bb182df2b516453907b3825ceaba0cb2a4efdccb7f2d97797e9e978001',1,'math.h']]],
  ['ghost_5fgemm_5fkahan',['GHOST_GEMM_KAHAN',['../math_8h.html#a5a51022bb182df2b516453907b3825ceaed062ae5c14e9cb681ba10974a05a19c',1,'math.h']]],
  ['ghost_5fgemm_5fnot_5fclone_5faliased',['GHOST_GEMM_NOT_CLONE_ALIASED',['../math_8h.html#a5a51022bb182df2b516453907b3825cead32d7dfc7c07fecdb1d83feb2aa2b5f4',1,'math.h']]],
  ['ghost_5fgemm_5fnot_5fspecial',['GHOST_GEMM_NOT_SPECIAL',['../math_8h.html#a5a51022bb182df2b516453907b3825cea10c1095fd3c9bfb7274d6a915b08d8c6',1,'math.h']]],
  ['ghost_5fimplementation_5favx',['GHOST_IMPLEMENTATION_AVX',['../types_8h.html#a431e3a747a7bc6db3c5b2d1e9decb656a2201271191035aa9909bc8d7387a19aa',1,'types.h']]],
  ['ghost_5fimplementation_5favx2',['GHOST_IMPLEMENTATION_AVX2',['../types_8h.html#a431e3a747a7bc6db3c5b2d1e9decb656a2a693dec7c94e0cce59bfa4af02f0c2d',1,'types.h']]],
  ['ghost_5fimplementation_5fcuda',['GHOST_IMPLEMENTATION_CUDA',['../types_8h.html#a431e3a747a7bc6db3c5b2d1e9decb656a82c0ffcbb43de6b126104c64c6603acc',1,'types.h']]],
  ['ghost_5fimplementation_5fdefault',['GHOST_IMPLEMENTATION_DEFAULT',['../types_8h.html#a431e3a747a7bc6db3c5b2d1e9decb656a2ce371f7bd6ebd33649d2831e7a32b56',1,'types.h']]],
  ['ghost_5fimplementation_5fmic',['GHOST_IMPLEMENTATION_MIC',['../types_8h.html#a431e3a747a7bc6db3c5b2d1e9decb656a9a0f91228d2c9caf8fa76886bbff3912',1,'types.h']]],
  ['ghost_5fimplementation_5fplain',['GHOST_IMPLEMENTATION_PLAIN',['../types_8h.html#a431e3a747a7bc6db3c5b2d1e9decb656a329da32b526df42ef8af9890c324b9f8',1,'types.h']]],
  ['ghost_5fimplementation_5fsse',['GHOST_IMPLEMENTATION_SSE',['../types_8h.html#a431e3a747a7bc6db3c5b2d1e9decb656af0b09dcd9e205078348ab8487cebb422',1,'types.h']]],
  ['ghost_5fkacz_5fdirection_5fbackward',['GHOST_KACZ_DIRECTION_BACKWARD',['../sparsemat_8h.html#abcdb5d4fbe1354296d99d535a20b0f4baf5b11af1fe0103c1b80dcb8761d269a4',1,'sparsemat.h']]],
  ['ghost_5fkacz_5fdirection_5fforward',['GHOST_KACZ_DIRECTION_FORWARD',['../sparsemat_8h.html#abcdb5d4fbe1354296d99d535a20b0f4ba54fd47952915a34396be0ec8c00830af',1,'sparsemat.h']]],
  ['ghost_5fkacz_5fdirection_5fundefined',['GHOST_KACZ_DIRECTION_UNDEFINED',['../sparsemat_8h.html#abcdb5d4fbe1354296d99d535a20b0f4baa4171471bc14d2c29efa94439aa4626c',1,'sparsemat.h']]],
  ['ghost_5fkacz_5fmethod_5fbmc',['GHOST_KACZ_METHOD_BMC',['../context_8h.html#a74e9d20610d604fb822bf96fa760b672a330c2a0f5a93e7934dfc5298237d170e',1,'context.h']]],
  ['ghost_5fkacz_5fmethod_5fbmc_5fone_5fsweep',['GHOST_KACZ_METHOD_BMC_one_sweep',['../context_8h.html#a74e9d20610d604fb822bf96fa760b672a3ceb1cfff4f5bdc543a8bb39eb7e44a4',1,'context.h']]],
  ['ghost_5fkacz_5fmethod_5fbmc_5frb',['GHOST_KACZ_METHOD_BMC_RB',['../context_8h.html#a74e9d20610d604fb822bf96fa760b672a6c32e5f84ed26158130a6b4ef31b8dbc',1,'context.h']]],
  ['ghost_5fkacz_5fmethod_5fbmc_5ftwo_5fsweep',['GHOST_KACZ_METHOD_BMC_two_sweep',['../context_8h.html#a74e9d20610d604fb822bf96fa760b672a7563f91244e01119d37dd43d5ea162f7',1,'context.h']]],
  ['ghost_5fkacz_5fmethod_5fbmcnormal',['GHOST_KACZ_METHOD_BMCNORMAL',['../context_8h.html#a74e9d20610d604fb822bf96fa760b672a58a97c1c9d8ef9a6171df70de121b2c6',1,'context.h']]],
  ['ghost_5fkacz_5fmethod_5fbmcshift',['GHOST_KACZ_METHOD_BMCshift',['../context_8h.html#a74e9d20610d604fb822bf96fa760b672a7cccd6bd0ec4c40c163e6ad5430ed65d',1,'context.h']]],
  ['ghost_5fkacz_5fmethod_5fmc',['GHOST_KACZ_METHOD_MC',['../context_8h.html#a74e9d20610d604fb822bf96fa760b672a2a5fc74d83e828b3778aba3fa4ec7052',1,'context.h']]],
  ['ghost_5fkacz_5fmode_5feco',['GHOST_KACZ_MODE_ECO',['../sparsemat_8h.html#ad777cdf60c973e8a6f12b3066ab5da6eac3d654f614cad544801185e51dfd663b',1,'sparsemat.h']]],
  ['ghost_5fkacz_5fmode_5fnormal',['GHOST_KACZ_MODE_NORMAL',['../sparsemat_8h.html#ad777cdf60c973e8a6f12b3066ab5da6ea6d99ecb7cfa6eead478c3cb2e91c7962',1,'sparsemat.h']]],
  ['ghost_5fkacz_5fmode_5fperformance',['GHOST_KACZ_MODE_PERFORMANCE',['../sparsemat_8h.html#ad777cdf60c973e8a6f12b3066ab5da6ea4db2e77c8177c30b2a28c58531783bdd',1,'sparsemat.h']]],
  ['ghost_5fkacz_5fnormalize_5fno',['GHOST_KACZ_NORMALIZE_NO',['../sparsemat_8h.html#a18c84f8abb39010db0d6f334b4d8a456ad192d021d3abb8d937a2625b54a0f109',1,'sparsemat.h']]],
  ['ghost_5fkacz_5fnormalize_5fsave',['GHOST_KACZ_NORMALIZE_SAVE',['../sparsemat_8h.html#a18c84f8abb39010db0d6f334b4d8a456a71b3c02e0efc23a5f18a026c24fd17ab',1,'sparsemat.h']]],
  ['ghost_5fkacz_5fnormalize_5fyes',['GHOST_KACZ_NORMALIZE_YES',['../sparsemat_8h.html#a18c84f8abb39010db0d6f334b4d8a456ae35b87f99c797ede7245acba394113ee',1,'sparsemat.h']]],
  ['ghost_5flocation_5fdefault',['GHOST_LOCATION_DEFAULT',['../types_8h.html#a1b80bcfd453b7cc7336cb3eb0b1f36e6a7c5be75d8e9fa3eda174353fea054a61',1,'types.h']]],
  ['ghost_5flocation_5fdevice',['GHOST_LOCATION_DEVICE',['../types_8h.html#a1b80bcfd453b7cc7336cb3eb0b1f36e6a1d95594d93ac192f27205ca26dc09cb9',1,'types.h']]],
  ['ghost_5flocation_5fhost',['GHOST_LOCATION_HOST',['../types_8h.html#a1b80bcfd453b7cc7336cb3eb0b1f36e6a2af45b817adef2363f2df9ec199f085a',1,'types.h']]],
  ['ghost_5fmap_5fcol',['GHOST_MAP_COL',['../map_8h.html#a2d63e7318821b5a7c22de2fd15e6647bab8ed68e81767f4b4c70698e2515933a1',1,'map.h']]],
  ['ghost_5fmap_5fdefault',['GHOST_MAP_DEFAULT',['../map_8h.html#a0ae0da15339136496fdd344698e60af4ac3875711d5dc344e92d0f99236d408bc',1,'map.h']]],
  ['ghost_5fmap_5fdist_5fnnz',['GHOST_MAP_DIST_NNZ',['../map_8h.html#a7c5bc1f548ace461e6d1a21e86219ba6af62da19ba62ec8b93a38c200390f54f6',1,'map.h']]],
  ['ghost_5fmap_5fdist_5fnrows',['GHOST_MAP_DIST_NROWS',['../map_8h.html#a7c5bc1f548ace461e6d1a21e86219ba6acb209e841922a7f7a755aa8cfd62faca',1,'map.h']]],
  ['ghost_5fmap_5fnone',['GHOST_MAP_NONE',['../map_8h.html#a2d63e7318821b5a7c22de2fd15e6647ba369da9f92ef7a2e8ca0ac5ab4f7c1e2d',1,'map.h']]],
  ['ghost_5fmap_5frow',['GHOST_MAP_ROW',['../map_8h.html#a2d63e7318821b5a7c22de2fd15e6647ba0097c3b26918f88d16bbfd7ec7f31f97',1,'map.h']]],
  ['ghost_5fperm_5fno_5fdistinction',['GHOST_PERM_NO_DISTINCTION',['../map_8h.html#a0ae0da15339136496fdd344698e60af4a9480114b71c98053f15ea80a0ebe00f5',1,'map.h']]],
  ['ghost_5fpermutation_5forig2perm',['GHOST_PERMUTATION_ORIG2PERM',['../context_8h.html#aa268d31786890eed06abb225888b637caa2f9a745f3a107e1343688fe87195dea',1,'context.h']]],
  ['ghost_5fpermutation_5fperm2orig',['GHOST_PERMUTATION_PERM2ORIG',['../context_8h.html#aa268d31786890eed06abb225888b637caff57d0dbfa67144e5a6a1fc6afb99f2f',1,'context.h']]],
  ['ghost_5frand_5fseed_5fnone',['GHOST_RAND_SEED_NONE',['../rand_8h.html#ad410b14ce5bbdaa7a0436aa563a8711ea11f305f64bdfac35984eb458b08a3021',1,'rand.h']]],
  ['ghost_5frand_5fseed_5fpu',['GHOST_RAND_SEED_PU',['../rand_8h.html#ad410b14ce5bbdaa7a0436aa563a8711ea2048430e3bcd0f4bb8f51b3fafd1a738',1,'rand.h']]],
  ['ghost_5frand_5fseed_5frank',['GHOST_RAND_SEED_RANK',['../rand_8h.html#ad410b14ce5bbdaa7a0436aa563a8711eaab0919552696b5dad2b1c384fcb8149a',1,'rand.h']]],
  ['ghost_5frand_5fseed_5ftime',['GHOST_RAND_SEED_TIME',['../rand_8h.html#ad410b14ce5bbdaa7a0436aa563a8711eacc5d3f22a71215f32cbfd1c3c01a00a7',1,'rand.h']]],
  ['ghost_5fsolver_5fkacz',['GHOST_SOLVER_KACZ',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba037532f8dd83863f633e4fc7993c78e1',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fblockcolor',['GHOST_SPARSEMAT_BLOCKCOLOR',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba7092566effd1ba38a7d166fd5b536015',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fcolor',['GHOST_SPARSEMAT_COLOR',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba151a5543e6724f5d0d77d4a689b8779b',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fdefault',['GHOST_SPARSEMAT_DEFAULT',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba0eefa5e81d4ba6a9af4ab32931706767',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fdevice',['GHOST_SPARSEMAT_DEVICE',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba7cc070be7d44983e587da38d1de99306',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fdiag_5ffirst',['GHOST_SPARSEMAT_DIAG_FIRST',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba7cd218654bbfcc8bb832d2376cdaf964',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fhost',['GHOST_SPARSEMAT_HOST',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba2b17062593adb47ad5dec05944480617',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fnot_5fpermute_5fcols',['GHOST_SPARSEMAT_NOT_PERMUTE_COLS',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804bac9f454c037265542ef932e3f129f62bb',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fnot_5fsort_5fcols',['GHOST_SPARSEMAT_NOT_SORT_COLS',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba05dd0f55d2797b852e6cc635385e807d',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fnot_5fstore_5ffull',['GHOST_SPARSEMAT_NOT_STORE_FULL',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba87869f260200fb1bdd079efa35edd775',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fnot_5fstore_5fsplit',['GHOST_SPARSEMAT_NOT_STORE_SPLIT',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba282299f527583dae7969e1ac4396d815',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fperm_5fno_5fdistinction',['GHOST_SPARSEMAT_PERM_NO_DISTINCTION',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba403ba038627a93e0fe89669d703cc6bd',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fpermute',['GHOST_SPARSEMAT_PERMUTE',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba0ffd94f14e0296b6aeacbd31f2799be1',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5frcm',['GHOST_SPARSEMAT_RCM',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804baf3360c8d4c9cdb6b1b40dcde456563e1',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5frowfunc_5fdefault',['GHOST_SPARSEMAT_ROWFUNC_DEFAULT',['../sparsemat__src_8h.html#a5f31f85685a7fb1990df73d59839c9e0ad0d2476939fb3b6f5bbb9fd8cbf05a4f',1,'sparsemat_src.h']]],
  ['ghost_5fsparsemat_5fsave_5forig_5fcols',['GHOST_SPARSEMAT_SAVE_ORIG_COLS',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba64cb044a1842fc9947bc88e828207ec7',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fscotchify',['GHOST_SPARSEMAT_SCOTCHIFY',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba4197b86ebd7597da099e19be42379839',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fsort_5frows',['GHOST_SPARSEMAT_SORT_ROWS',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804baecf691067c1f77f3b5b521f2dbb16541',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fsrc_5ffile',['GHOST_SPARSEMAT_SRC_FILE',['../map_8h.html#a0f70835b107b60fb690b57dba7b64a4da8987728b06e0aa58436863e0a569dce6',1,'map.h']]],
  ['ghost_5fsparsemat_5fsrc_5ffunc',['GHOST_SPARSEMAT_SRC_FUNC',['../map_8h.html#a0f70835b107b60fb690b57dba7b64a4da7c085ca1c6e32cdda5b160fed1850c8f',1,'map.h']]],
  ['ghost_5fsparsemat_5fsrc_5fmm',['GHOST_SPARSEMAT_SRC_MM',['../map_8h.html#a0f70835b107b60fb690b57dba7b64a4da4d4ef0abc2ea7f752a7902aa8308d0d9',1,'map.h']]],
  ['ghost_5fsparsemat_5fsrc_5fnone',['GHOST_SPARSEMAT_SRC_NONE',['../map_8h.html#a0f70835b107b60fb690b57dba7b64a4da39f536121db562bb5aeb534d3a380630',1,'map.h']]],
  ['ghost_5fsparsemat_5fsymm_5fgeneral',['GHOST_SPARSEMAT_SYMM_GENERAL',['../sparsemat_8h.html#a99084a5ccef27853a473f8b86ff9ecc6aa2f1be44efe2721a7b059af0845604cc',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fsymm_5fhermitian',['GHOST_SPARSEMAT_SYMM_HERMITIAN',['../sparsemat_8h.html#a99084a5ccef27853a473f8b86ff9ecc6aa78f0d55ceb12abbf739433ea1e9ed58',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fsymm_5fskew_5fsymmetric',['GHOST_SPARSEMAT_SYMM_SKEW_SYMMETRIC',['../sparsemat_8h.html#a99084a5ccef27853a473f8b86ff9ecc6ae42ea12e08dc6f2257fa1e2db339df8e',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fsymm_5fsymmetric',['GHOST_SPARSEMAT_SYMM_SYMMETRIC',['../sparsemat_8h.html#a99084a5ccef27853a473f8b86ff9ecc6a2c141045e13d9b11357d17d5c643b45a',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5ftranspose_5fmm',['GHOST_SPARSEMAT_TRANSPOSE_MM',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804bab14ab3eb592b2e204013bde8912ba763',1,'sparsemat.h']]],
  ['ghost_5fsparsemat_5fzoltan',['GHOST_SPARSEMAT_ZOLTAN',['../sparsemat_8h.html#a6b2a7e07968316b245f8f964dda7804ba5c362d4bd95755d5ecfd0420ee6479ec',1,'sparsemat.h']]],
  ['ghost_5fspmv_5faxpby',['GHOST_SPMV_AXPBY',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000ae3552422301cc35e4148465986f3417e',1,'spmv.h']]],
  ['ghost_5fspmv_5faxpy',['GHOST_SPMV_AXPY',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000a7b586ddce89f3933d6703e45aea67a23',1,'spmv.h']]],
  ['ghost_5fspmv_5fbarrier',['GHOST_SPMV_BARRIER',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000a512059b3a597e5ed2f235307c70bb49f',1,'spmv.h']]],
  ['ghost_5fspmv_5fchain_5faxpby',['GHOST_SPMV_CHAIN_AXPBY',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000a2e16f1c14c033c7ab3185918da8fc98f',1,'spmv.h']]],
  ['ghost_5fspmv_5fdefault',['GHOST_SPMV_DEFAULT',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000a9516c0b8b972839a0ac6339fc64e6f7b',1,'spmv.h']]],
  ['ghost_5fspmv_5fdot_5fxx',['GHOST_SPMV_DOT_XX',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000a383b72594bf1946709172dc8f32a4ab1',1,'spmv.h']]],
  ['ghost_5fspmv_5fdot_5fxy',['GHOST_SPMV_DOT_XY',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000ad928fd1401e5a0cabaaebb8a0f489c75',1,'spmv.h']]],
  ['ghost_5fspmv_5fdot_5fyy',['GHOST_SPMV_DOT_YY',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000aa739570a0f4b6b1a78e447dd8e5ffba1',1,'spmv.h']]],
  ['ghost_5fspmv_5flocal',['GHOST_SPMV_LOCAL',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000a87aaf8efe5ee80f70e0c7667d6b4612d',1,'spmv.h']]],
  ['ghost_5fspmv_5fmode_5fnocomm',['GHOST_SPMV_MODE_NOCOMM',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000af560e5119aae07a1d2600e51680c3d8d',1,'spmv.h']]],
  ['ghost_5fspmv_5fmode_5foverlap',['GHOST_SPMV_MODE_OVERLAP',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000a151a965750758d091ac23605cfc5cb9c',1,'spmv.h']]],
  ['ghost_5fspmv_5fmode_5fpipelined',['GHOST_SPMV_MODE_PIPELINED',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000a63509c1d09298e9fbcd0a83322ec057b',1,'spmv.h']]],
  ['ghost_5fspmv_5fmode_5ftask',['GHOST_SPMV_MODE_TASK',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000a089686942c09136e5ca9176bccc5009d',1,'spmv.h']]],
  ['ghost_5fspmv_5fnot_5freduce',['GHOST_SPMV_NOT_REDUCE',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000aa289a7dbc074dba11b61fdee27df5ff3',1,'spmv.h']]],
  ['ghost_5fspmv_5fremote',['GHOST_SPMV_REMOTE',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000a6338180ba63f6a308032cd8d03bdace5',1,'spmv.h']]],
  ['ghost_5fspmv_5fscale',['GHOST_SPMV_SCALE',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000a8432938c7a80a743d662182db94f079e',1,'spmv.h']]],
  ['ghost_5fspmv_5fshift',['GHOST_SPMV_SHIFT',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000a8933c03f14130a08e2410eb6dc629a10',1,'spmv.h']]],
  ['ghost_5fspmv_5fvshift',['GHOST_SPMV_VSHIFT',['../spmv_8h.html#a366982eb9bbe69dbb7eed476381bc000a49e3a9c30077d6290dbe20a3f41cd5c3',1,'spmv.h']]],
  ['ghost_5fsuccess',['GHOST_SUCCESS',['../error_8h.html#aeeb8927260b4a186dfc5f72ebe0055dfa6c8168f88760ce826df1376fb40502a9',1,'error.h']]],
  ['ghost_5ftask_5fcreated',['GHOST_TASK_CREATED',['../task_8h.html#a82b9fa33342b1ea3bf0e42e0112b751ea970b30a67b603204d023aec75a45aa1c',1,'task.h']]],
  ['ghost_5ftask_5fdefault',['GHOST_TASK_DEFAULT',['../task_8h.html#a8d6053a35f7151cd63044780a3571f9ca0b536b829578383c92699b5012ac04aa',1,'task.h']]],
  ['ghost_5ftask_5fenqueued',['GHOST_TASK_ENQUEUED',['../task_8h.html#a82b9fa33342b1ea3bf0e42e0112b751ea55940a857c7f762ddb7c05f7e1bf7624',1,'task.h']]],
  ['ghost_5ftask_5ffinished',['GHOST_TASK_FINISHED',['../task_8h.html#a82b9fa33342b1ea3bf0e42e0112b751ea2e287f3790423a7935bc4c276c144053',1,'task.h']]],
  ['ghost_5ftask_5finvalid',['GHOST_TASK_INVALID',['../task_8h.html#a82b9fa33342b1ea3bf0e42e0112b751ea1d63dcee87c15a1b980bcfd208b7e832',1,'task.h']]],
  ['ghost_5ftask_5fld_5fstrict',['GHOST_TASK_LD_STRICT',['../task_8h.html#a8d6053a35f7151cd63044780a3571f9cafec7526c9db3cb2290813e6e78d4946b',1,'task.h']]],
  ['ghost_5ftask_5fno_5fhyperthreads',['GHOST_TASK_NO_HYPERTHREADS',['../task_8h.html#a8d6053a35f7151cd63044780a3571f9ca2f7bd3a1dc4e8c9b6834b7993c25320d',1,'task.h']]],
  ['ghost_5ftask_5fnot_5fallow_5fchild',['GHOST_TASK_NOT_ALLOW_CHILD',['../task_8h.html#a8d6053a35f7151cd63044780a3571f9ca1f9bdcfe99b72a685b64eda027f0c413',1,'task.h']]],
  ['ghost_5ftask_5fnot_5fpin',['GHOST_TASK_NOT_PIN',['../task_8h.html#a8d6053a35f7151cd63044780a3571f9ca5210bc810b4dac73a072179df1c814ab',1,'task.h']]],
  ['ghost_5ftask_5fonly_5fhyperthreads',['GHOST_TASK_ONLY_HYPERTHREADS',['../task_8h.html#a8d6053a35f7151cd63044780a3571f9cadff8f8e5f3b17c583aba669dddb29399',1,'task.h']]],
  ['ghost_5ftask_5fprio_5fhigh',['GHOST_TASK_PRIO_HIGH',['../task_8h.html#a8d6053a35f7151cd63044780a3571f9ca1e0b358ec2b97f45f1ca647034039629',1,'task.h']]],
  ['ghost_5ftask_5frunning',['GHOST_TASK_RUNNING',['../task_8h.html#a82b9fa33342b1ea3bf0e42e0112b751ea920a0ddd6e4ec1650f1ce946b8540f0a',1,'task.h']]],
  ['ghost_5ftype_5fcuda',['GHOST_TYPE_CUDA',['../group__core.html#gga8cb15e3e632c7da225baae63ce4ccdadafb69d5413ccff849d7c3d4922365779b',1,'core.h']]],
  ['ghost_5ftype_5finvalid',['GHOST_TYPE_INVALID',['../group__core.html#gga8cb15e3e632c7da225baae63ce4ccdada3d899047d142a82656460462d8851d2d',1,'core.h']]],
  ['ghost_5ftype_5fwork',['GHOST_TYPE_WORK',['../group__core.html#gga8cb15e3e632c7da225baae63ce4ccdadab4fb116818bc69f23c39c16a44a29ec9',1,'core.h']]],
  ['ghost_5funaligned',['GHOST_UNALIGNED',['../types_8h.html#ac2562a957b35f810f4f24f2eeb9448faa6a07806d8c17b169a55b418dc882afbc',1,'types.h']]]
];
