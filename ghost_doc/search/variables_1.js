var searchData=
[
  ['b',['B',['../structghost__compatible__vec__vec.html#adc2d8d3372731b706edc3fbcbef7cec2',1,'ghost_compatible_vec_vec']]],
  ['bandwidth',['bandwidth',['../structghost__context.html#ae2b7a7966db170bb76cc9c74c2d76b06',1,'ghost_context']]],
  ['base',['base',['../structghost__bincrs__header__t.html#a9bbbb179045388bb2bd37e22cd8fc8bb',1,'ghost_bincrs_header_t::base()'],['../structghost__sparsemat__src__rowfunc.html#a6407f2af232389069cca6beceacaabf3',1,'ghost_sparsemat_src_rowfunc::base()']]],
  ['best_5fblock_5fsize',['best_block_size',['../structghost__kacz__opts.html#a98c0912686a2c78d152f9080e0aed6fb',1,'ghost_kacz_opts::best_block_size()'],['../structghost__carp__opts.html#a390e76fb908f7c4c91a86e57bdfcda15',1,'ghost_carp_opts::best_block_size()']]],
  ['beta',['beta',['../structghost__spmv__opts.html#a1d77a300c25fecfc6fb04e6653fd8a3b',1,'ghost_spmv_opts']]],
  ['betaiszero',['betaiszero',['../structghost__gemm__perf__args.html#a4b2a9821552389f2c1da567082928cb1',1,'ghost_gemm_perf_args']]],
  ['blocklen',['blocklen',['../structghost__densemat.html#a392f7a3e635d10eca1814681d85b6995',1,'ghost_densemat']]],
  ['blocksz',['blocksz',['../structghost__dot__parameters.html#a7fb08e1b38dbbb6e503c9268e9cc7ecb',1,'ghost_dot_parameters::blocksz()'],['../structghost__spmv__opts.html#a5bd2947f5ad4fe8680b4273ddc8d008c',1,'ghost_spmv_opts::blocksz()'],['../structghost__sellspmv__parameters.html#a39eb90a6ad9f4ba5e085eadbcaf25dcc',1,'ghost_sellspmv_parameters::blocksz()'],['../structghost__cusellspmv__parameters.html#a27c9b0169a4cb7d18b8f53a5d2da77ac',1,'ghost_cusellspmv_parameters::blocksz()'],['../structghost__kacz__parameters.html#ac7ad3e1012e9b489e322167a77c4d213',1,'ghost_kacz_parameters::blocksz()']]],
  ['buffers',['buffers',['../cu__temp__buffer__malloc_8cpp.html#afa0c25102ae46926713af811745b4c0e',1,'cu_temp_buffer_malloc.cpp']]],
  ['busy',['busy',['../structghost__pumap.html#a3afc9d9e3736fe402a32556f3829850c',1,'ghost_pumap']]]
];
