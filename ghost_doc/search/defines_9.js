var searchData=
[
  ['labelwidth',['LABELWIDTH',['../util_8c.html#a5a7195b7b04722d0ecfd28d5b8be3940',1,'util.c']]],
  ['local_5fhostname_5fmax',['LOCAL_HOSTNAME_MAX',['../locality_8c.html#acdb24cdbfe01e7168af56fdab3dd95aa',1,'locality.c']]],
  ['localdot_5fonthefly',['LOCALDOT_ONTHEFLY',['../sell__spmv__cu__kernel_8h.html#a14d96c6552f6926bb21428605226bbb6',1,'sell_spmv_cu_kernel.h']]],
  ['lock_5fneighbour',['LOCK_NEIGHBOUR',['../sell__kacz__bmc_8c.html#aa95df7fa62f175e4689135306c49d62f',1,'LOCK_NEIGHBOUR():&#160;sell_kacz_bmc.c'],['../sell__kacz__fallback_8cpp.html#aa95df7fa62f175e4689135306c49d62f',1,'LOCK_NEIGHBOUR():&#160;sell_kacz_fallback.cpp']]],
  ['loop',['LOOP',['../sell__kacz__bmc_8c.html#a0bab189657cfd87176d8429fde64c93d',1,'sell_kacz_bmc.c']]],
  ['loop_5fin_5fchunk',['LOOP_IN_CHUNK',['../sell__kacz__fallback_8cpp.html#a24979eebdab26e5939cf3e6a0829bacd',1,'sell_kacz_fallback.cpp']]],
  ['loopinchunk',['LOOPINCHUNK',['../sell__kacz__mc_8c.html#a82afd5b73c020cff3e09c9cb2a4accb7',1,'sell_kacz_mc.c']]]
];
