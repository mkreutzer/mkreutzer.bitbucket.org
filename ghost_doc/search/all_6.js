var searchData=
[
  ['filename',['filename',['../structghost__sparsemat__rowfunc__file__initargs.html#a53f5c292f440e400e63a1f89ab732e82',1,'ghost_sparsemat_rowfunc_file_initargs']]],
  ['find_5ftransition_5fzone',['find_transition_zone',['../rcm__dissection_8cpp.html#a013f33b4647a452b4d27ebdeb15d3445',1,'find_transition_zone(ghost_sparsemat *mat, int n_threads):&#160;rcm_dissection.cpp'],['../rcm__dissection_8h.html#a013f33b4647a452b4d27ebdeb15d3445',1,'find_transition_zone(ghost_sparsemat *mat, int n_threads):&#160;rcm_dissection.cpp']]],
  ['find_5fzone_5fextrema',['find_zone_extrema',['../kacz__hybrid__split_8c.html#a7a0387acc9c4acd0c48472c29e741982',1,'find_zone_extrema(ghost_sparsemat *mat, int **extrema, ghost_lidx a, ghost_lidx b):&#160;kacz_hybrid_split.c'],['../kacz__hybrid__split_8h.html#a7a0387acc9c4acd0c48472c29e741982',1,'find_zone_extrema(ghost_sparsemat *mat, int **extrema, ghost_lidx a, ghost_lidx b):&#160;kacz_hybrid_split.c']]],
  ['finishedcond',['finishedCond',['../structghost__task.html#ad6af4821eff3fa99442620268c13d43a',1,'ghost_task']]],
  ['finishedmutex',['finishedMutex',['../structghost__task.html#ac140d27155d1d3e599468ff003a97195',1,'ghost_task']]],
  ['flags',['flags',['../structghost__context.html#a597960a06b866275ea992d6e2ebf3e81',1,'ghost_context::flags()'],['../structghost__densemat__traits.html#a4f174e9d421e7a868b1d6fcfa8a54a2f',1,'ghost_densemat_traits::flags()'],['../structghost__map.html#a14d14a0c8fa6289e5603c96df43bfadc',1,'ghost_map::flags()'],['../structghost__spmv__perf__args.html#a694e55a4d0608fc230a7bc9452aaa188',1,'ghost_spmv_perf_args::flags()'],['../structghost__spmv__opts.html#a0141f518d64511cb1c092f7769d5aefd',1,'ghost_spmv_opts::flags()'],['../structghost__sparsemat__traits.html#af33c829da5726d9d9e0aca31ad581458',1,'ghost_sparsemat_traits::flags()'],['../structghost__sparsemat__src__rowfunc.html#ae915ffbb3cd2f3ee356990836501b54b',1,'ghost_sparsemat_src_rowfunc::flags()'],['../structghost__task.html#aea95a1d433dd5efda57d13e797699895',1,'ghost_task::flags()']]],
  ['footerheight',['FOOTERHEIGHT',['../util_8c.html#a8246315176042655077afabc0bf40231',1,'util.c']]],
  ['forward_5floop',['FORWARD_LOOP',['../sell__kacz__bmc_8c.html#ada4bfb9714740195dce831f288a39766',1,'FORWARD_LOOP():&#160;sell_kacz_bmc.c'],['../sell__kacz__fallback_8cpp.html#a3a424622d61489c1393fc614bf3dec68',1,'FORWARD_LOOP():&#160;sell_kacz_fallback.cpp']]],
  ['forward_5fshift_5floop',['FORWARD_SHIFT_LOOP',['../sell__kacz__bmc_8c.html#a815d9c2e0940c3ef49ba5aadfbaeb980',1,'sell_kacz_bmc.c']]],
  ['freed',['freed',['../structghost__task.html#a5219bbc0cb872aed5175b5d94db375ac',1,'ghost_task']]],
  ['fromreal',['fromReal',['../cu__complex_8h.html#a9e1135e4f2daa55a35870410a0ee416f',1,'cu_complex.h']]],
  ['fromreal_3c_20cudoublecomplex_2c_20double_20_3e',['fromReal&lt; cuDoubleComplex, double &gt;',['../cu__complex_8h.html#a4ace48ba7b39f36a8d0dda2bd1f50323',1,'cu_complex.h']]],
  ['fromreal_3c_20cufloatcomplex_2c_20float_20_3e',['fromReal&lt; cuFloatComplex, float &gt;',['../cu__complex_8h.html#a0883ee3c98db9dc30e2ba1a07ac4c697',1,'cu_complex.h']]],
  ['fullmpidt',['fullmpidt',['../structghost__densemat.html#aa3ca6d181251a1cd3f913b18f4d87ade',1,'ghost_densemat']]],
  ['func',['func',['../structghost__sparsemat__src__rowfunc.html#a192953e6f3938416fdf293110424b6be',1,'ghost_sparsemat_src_rowfunc::func()'],['../structghost__task.html#aeb72fc017e378379621eb1aa69c9fa07',1,'ghost_task::func()']]],
  ['funcinit',['funcinit',['../structghost__sparsemat__src__rowfunc.html#a8c7c7e802ab69ed1d63185450c084a90',1,'ghost_sparsemat_src_rowfunc']]]
];
