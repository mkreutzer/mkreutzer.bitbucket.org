var searchData=
[
  ['scotch_5fcall',['SCOTCH_CALL',['../error_8h.html#a2de583b2d64ce059cf1ec79fd8fb2ad2',1,'error.h']]],
  ['scotch_5fcall_5fgoto',['SCOTCH_CALL_GOTO',['../error_8h.html#a0cfe83b37c31a21a2ec026512c478403',1,'error.h']]],
  ['scotch_5fcall_5freturn',['SCOTCH_CALL_RETURN',['../error_8h.html#ad2521ff30c8a98e2e80369c558b52419',1,'error.h']]],
  ['select_5f20th',['SELECT_20TH',['../log_8h.html#ae04eae8c049646898d0fb09ab6be5d8e',1,'log.h']]],
  ['select_5fblas1_5fkernel',['SELECT_BLAS1_KERNEL',['../math_8h.html#ab47f3bf0e383bb6429787885e6c93980',1,'math.h']]],
  ['select_5ftmpl_5f1datatype',['SELECT_TMPL_1DATATYPE',['../types_8h.html#ab4c28f320917e2b757a409a13d97c061',1,'types.h']]],
  ['select_5ftmpl_5f2datatypes',['SELECT_TMPL_2DATATYPES',['../types_8h.html#af91a262a5f40613cefcd5a3ea7301ac6',1,'types.h']]],
  ['select_5ftmpl_5f2datatypes_5fbase_5fderived',['SELECT_TMPL_2DATATYPES_base_derived',['../types_8h.html#a427f3910cca954e15875115dbe1e29d5',1,'types.h']]],
  ['select_5ftmpl_5f4datatypes',['SELECT_TMPL_4DATATYPES',['../types_8h.html#ab2beb2e49ed198c651863bfa793baacb',1,'types.h']]],
  ['sell_5fcuda_5fthreadsperblock',['SELL_CUDA_THREADSPERBLOCK',['../sell__spmv__cu__kernel_8h.html#ab37ea0b557bd6ae08ca58feac90675f2',1,'sell_spmv_cu_kernel.h']]],
  ['sgemm',['sgemm',['../blas__mangle_8h.html#ae1b98caeb3c97c88d456ae813a3b52a0',1,'blas_mangle.h']]],
  ['shift_5floop',['SHIFT_LOOP',['../sell__kacz__bmc_8c.html#ada041acf16e15387751fbf1a722e3807',1,'sell_kacz_bmc.c']]],
  ['spm_5fgncols',['SPM_GNCOLS',['../sparsemat_8h.html#ac9c99e9b10e257166fb35dbf9eb62a75',1,'sparsemat.h']]],
  ['spm_5fnchunks',['SPM_NCHUNKS',['../sparsemat_8h.html#ab243360ad4d80f91277baf77d62f1352',1,'sparsemat.h']]],
  ['spm_5fncols',['SPM_NCOLS',['../sparsemat_8h.html#a85dea570886b9c98fd2a033dabbb425d',1,'sparsemat.h']]],
  ['spm_5fnnz',['SPM_NNZ',['../sparsemat_8h.html#a5475e1141e28f60257fb2bb6c3f6bddb',1,'sparsemat.h']]],
  ['spm_5fnrows',['SPM_NROWS',['../sparsemat_8h.html#ab24c01d5ee27ab08d16cc2f22b18c968',1,'sparsemat.h']]],
  ['spm_5fnrowspad',['SPM_NROWSPAD',['../sparsemat_8h.html#a8637bf4a0971200ab75b9b5d4ad27b1f',1,'sparsemat.h']]],
  ['swapreq',['SWAPREQ',['../bincrs__func_8c.html#ae6fa4c3ed2a85aa517c940604cb4a297',1,'bincrs_func.c']]]
];
