var searchData=
[
  ['c',['C',['../structghost__compatible__vec__vec.html#a04917a7f2129ee8259b79ef690511537',1,'ghost_compatible_vec_vec::C()'],['../structghost__sparsemat__traits.html#a2900878a44bb724485a907b27662cf24',1,'ghost_sparsemat_traits::C()']]],
  ['calculate_5fbw',['calculate_bw',['../sparsemat_8h.html#a35485c6fdfb7fa40f54827b13f19a4cd',1,'sparsemat.h']]],
  ['call_5fdensemat_5ffunc',['CALL_DENSEMAT_FUNC',['../densemat_8c.html#a0a5f7c4cb089ed30990f08efc55444cd',1,'densemat.c']]],
  ['call_5fdensemat_5ffunc_5fnoret',['CALL_DENSEMAT_FUNC_NORET',['../densemat_8c.html#ad34e749fb7bc96c29ac01336bd9b3b41',1,'CALL_DENSEMAT_FUNC_NORET():&#160;densemat.c'],['../densemat_8cpp.html#ad34e749fb7bc96c29ac01336bd9b3b41',1,'CALL_DENSEMAT_FUNC_NORET():&#160;densemat.cpp']]],
  ['carp_2ec',['carp.c',['../carp_8c.html',1,'']]],
  ['carp_2eh',['carp.h',['../carp_8h.html',1,'']]],
  ['carp_5frb_2ec',['carp_rb.c',['../carp__rb_8c.html',1,'']]],
  ['carp_5frb_2eh',['carp_rb.h',['../carp__rb_8h.html',1,'']]],
  ['cc_5fghost_5fcastarray',['cc_ghost_castarray',['../bincrs_8cpp.html#a5d223e8fbb767fe1241a883e27e7d625',1,'bincrs.cpp']]],
  ['cd_5fghost_5fcastarray',['cd_ghost_castarray',['../bincrs_8cpp.html#a9f843f206fefbbbaa1feb4549b0b36fd',1,'bincrs.cpp']]],
  ['ceildiv',['CEILDIV',['../util_8h.html#ac10af5b68e4ce4cd01d1f2f2cfdfdebb',1,'util.h']]],
  ['cgemm',['cgemm',['../blas__mangle_8h.html#a719025f93b73710ac468c1eb6b49293a',1,'blas_mangle.h']]],
  ['charfield2string',['charfield2string',['../densemat_8c.html#a6112f0d07626de48bde738756d7f2dca',1,'densemat.c']]],
  ['checker',['checker',['../kacz__hybrid__split_8c.html#a9c82e0d582c8f898cf2c1a90f0cfe3c0',1,'checker(ghost_sparsemat *mat):&#160;kacz_hybrid_split.c'],['../kacz__hybrid__split_8h.html#a9c82e0d582c8f898cf2c1a90f0cfe3c0',1,'checker(ghost_sparsemat *mat):&#160;kacz_hybrid_split.c'],['../sparsemat_8h.html#a9c82e0d582c8f898cf2c1a90f0cfe3c0',1,'checker(ghost_sparsemat *mat):&#160;kacz_hybrid_split.c']]],
  ['checker_5frcm',['checker_rcm',['../rcm__dissection_8cpp.html#ad8b685635df8324fcf8a35c05084ed78',1,'checker_rcm(ghost_sparsemat *mat):&#160;rcm_dissection.cpp'],['../rcm__dissection_8h.html#ad8b685635df8324fcf8a35c05084ed78',1,'checker_rcm(ghost_sparsemat *mat):&#160;rcm_dissection.cpp']]],
  ['childusedmap',['childusedmap',['../structghost__task.html#a85b26a50cd51609d11b5bd03399b9bb6',1,'ghost_task']]],
  ['chunkheight',['chunkheight',['../structghost__sellspmv__parameters.html#ac169ec017cad66a8e5c8363856b29154',1,'ghost_sellspmv_parameters::chunkheight()'],['../structghost__cusellspmv__parameters.html#ae7916e5cc35c9171c61bd0d4adf9c59e',1,'ghost_cusellspmv_parameters::chunkheight()'],['../structghost__kacz__parameters.html#a7092a4b6dc17849aa615bebdb979d2e1',1,'ghost_kacz_parameters::chunkheight()'],['../sell__kacz__bmc_8c.html#aa054b80a7e052b3cb9c90856f8c71deb',1,'CHUNKHEIGHT():&#160;sell_kacz_bmc.c']]],
  ['chunklen',['chunkLen',['../structghost__sparsemat.html#af6ad0b95d83c325ee4c6b3bcc7cd416d',1,'ghost_sparsemat']]],
  ['chunklenpadded',['chunkLenPadded',['../structghost__sparsemat.html#ad3c300756869ea4ab0a6eef3885d4334',1,'ghost_sparsemat']]],
  ['chunkmin',['chunkMin',['../structghost__sparsemat.html#ae68a230a526d03359f5cd4bb71343756',1,'ghost_sparsemat']]],
  ['chunkstart',['chunkStart',['../structghost__sparsemat.html#a277a151bf6ffb555f2b63d1bb3e8c117',1,'ghost_sparsemat']]],
  ['cm_5ffuncname',['CM_FUNCNAME',['../densemat_8c.html#ab3a7cca203d33407af62ef0a5c167602',1,'CM_FUNCNAME():&#160;densemat.c'],['../densemat_8cpp.html#ab3a7cca203d33407af62ef0a5c167602',1,'CM_FUNCNAME():&#160;densemat.cpp']]],
  ['codegeneration_2emd',['codegeneration.md',['../codegeneration_8md.html',1,'']]],
  ['col',['col',['../structghost__sparsemat__rowfunc__crs__arg.html#a18c9556e22c891e56fb84bfeb98279d5',1,'ghost_sparsemat_rowfunc_crs_arg::col()'],['../structghost__sparsemat.html#a389e59a6a9eba777aa0574fa9e8044b6',1,'ghost_sparsemat::col()']]],
  ['col_5fmap',['col_map',['../structghost__context.html#ab3d7b62a109904f6b40af31855f30307',1,'ghost_context']]],
  ['col_5forig',['col_orig',['../structghost__sparsemat.html#ac350d14abfb282c53b5610ed0e58f4af',1,'ghost_sparsemat']]],
  ['colmajor',['COLMAJOR',['../densemat__cm_8c.html#a5f9397733c8d92b28e2d3824c3d159e9',1,'COLMAJOR():&#160;densemat_cm.c'],['../densemat__cm_8cpp.html#a5f9397733c8d92b28e2d3824c3d159e9',1,'COLMAJOR():&#160;densemat_cm.cpp'],['../densemat__cm__averagehalo_8cpp.html#a5f9397733c8d92b28e2d3824c3d159e9',1,'COLMAJOR():&#160;densemat_cm_averagehalo.cpp']]],
  ['colmask',['colmask',['../structghost__densemat.html#a3b589f2ff04ea62e6f5e78cfa30a2d9b',1,'ghost_densemat']]],
  ['coloff',['coloff',['../structghost__densemat.html#a6a1bfe72e08822f6ba065b0ede00fc6e',1,'ghost_densemat']]],
  ['color_5fptr',['color_ptr',['../structghost__context.html#aae749fe0c5251cca97bd6f926953ab63',1,'ghost_context']]],
  ['colpack_5fcall',['COLPACK_CALL',['../error_8h.html#aac03122fb184398402ce6bbf0827a349',1,'error.h']]],
  ['colpack_5fcall_5fgoto',['COLPACK_CALL_GOTO',['../error_8h.html#a3847b68989e5debbb77c932e8fb37ba4',1,'error.h']]],
  ['colpack_5fcall_5freturn',['COLPACK_CALL_RETURN',['../error_8h.html#af2bc9629772cde4754cbe451ad86169e',1,'error.h']]],
  ['compatibility_5fcheck_2ec',['compatibility_check.c',['../compatibility__check_8c.html',1,'']]],
  ['compatibility_5fcheck_2eh',['compatibility_check.h',['../compatibility__check_8h.html',1,'']]],
  ['compute_5fat',['compute_at',['../structghost__densemat__traits.html#a5168406c958f54fa5c95535e748865c3',1,'ghost_densemat_traits']]],
  ['compute_5fwith',['compute_with',['../structghost__densemat__traits.html#a79137bfe35c37d751eaa27fff48b469d',1,'ghost_densemat_traits']]],
  ['conj',['conj',['../cu__complex_8h.html#a3b6f27fad013a8c1a67d0ddc9ff4b6a4',1,'cu_complex.h']]],
  ['conj_3c_20cudoublecomplex_20_3e',['conj&lt; cuDoubleComplex &gt;',['../cu__complex_8h.html#a59ce0abc0f20d9c28236741ba2de1b20',1,'cu_complex.h']]],
  ['conj_3c_20cufloatcomplex_20_3e',['conj&lt; cuFloatComplex &gt;',['../cu__complex_8h.html#a10029592a73be3d55a0aea6d8840830f',1,'cu_complex.h']]],
  ['conj_5for_5fnop',['conj_or_nop',['../namespaceghost.html#a0c5ffcacb6a285105e12124133036bd4',1,'ghost::conj_or_nop(const T &amp;a)'],['../namespaceghost.html#a050d515d5deca4a1acf254479e5cf4f6',1,'ghost::conj_or_nop(const float &amp;a)'],['../namespaceghost.html#a706c93b8e658e91c2582879950e5786a',1,'ghost::conj_or_nop(const double &amp;a)']]],
  ['constants_2eh',['constants.h',['../constants_8h.html',1,'']]],
  ['context',['context',['../structghost__sparsemat.html#ad0d67ffc1567d8e667efed822b425ea7',1,'ghost_sparsemat']]],
  ['context_2ec',['context.c',['../context_8c.html',1,'']]],
  ['context_2ecpp',['context.cpp',['../context_8cpp.html',1,'']]],
  ['context_2eh',['context.h',['../context_8h.html',1,'']]],
  ['context_2emd',['context.md',['../context_8md.html',1,'']]],
  ['core_20functionality',['Core functionality',['../group__core.html',1,'']]],
  ['core_2ec',['core.c',['../core_8c.html',1,'']]],
  ['core_2eh',['core.h',['../core_8h.html',1,'']]],
  ['coremap',['coremap',['../structghost__task.html#aea75e227ebdaa65b7464838beb75f532',1,'ghost_task']]],
  ['cpp11_5ffixes_2eh',['cpp11_fixes.h',['../cpp11__fixes_8h.html',1,'']]],
  ['cpuid',['cpuid',['../machine_8c.html#a469d2e95c1f7d083f28066725a61ee88',1,'machine.c']]],
  ['cpuset',['cpuset',['../structghost__pumap.html#a138295a1deda6ef7275d95b9c84ff1e8',1,'ghost_pumap']]],
  ['cs_5fghost_5fcastarray',['cs_ghost_castarray',['../bincrs_8cpp.html#abf30b06963bcda2abcc15a231ba5920c',1,'bincrs.cpp']]],
  ['cu_5fbench_2eh',['cu_bench.h',['../cu__bench_8h.html',1,'']]],
  ['cu_5fchunklen',['cu_chunkLen',['../structghost__sparsemat.html#aba0b8d6b9847b52ca1fed8e261d4561e',1,'ghost_sparsemat']]],
  ['cu_5fchunkstart',['cu_chunkStart',['../structghost__sparsemat.html#a0819c7879ac6d267a20c02681b40e91c',1,'ghost_sparsemat']]],
  ['cu_5fcol',['cu_col',['../structghost__sparsemat.html#afaeb22d22ecb26854803f77764d23660',1,'ghost_sparsemat']]],
  ['cu_5fcomplex_2eh',['cu_complex.h',['../cu__complex_8h.html',1,'']]],
  ['cu_5fdensemat_5fcm_2eh',['cu_densemat_cm.h',['../cu__densemat__cm_8h.html',1,'']]],
  ['cu_5fdensemat_5frm_2eh',['cu_densemat_rm.h',['../cu__densemat__rm_8h.html',1,'']]],
  ['cu_5fduelist',['cu_duelist',['../structghost__context.html#a582e037fd81dbe894b9c05d0f8953b3d',1,'ghost_context']]],
  ['cu_5floc_5fperm',['cu_loc_perm',['../structghost__map.html#a3366916baf176026e31fbf49d055535b',1,'ghost_map']]],
  ['cu_5frowlen',['cu_rowLen',['../structghost__sparsemat.html#afbb4af873f86fc3358ca69ae744677ce',1,'ghost_sparsemat']]],
  ['cu_5frowlenpadded',['cu_rowLenPadded',['../structghost__sparsemat.html#a082928ca41cf5b5d7779266b4534e354',1,'ghost_sparsemat']]],
  ['cu_5fseed',['cu_seed',['../rand_8c.html#a400bee245b64913da115b0373f0cab18',1,'rand.c']]],
  ['cu_5fsell_5fkernel_2eh',['cu_sell_kernel.h',['../cu__sell__kernel_8h.html',1,'']]],
  ['cu_5ftemp_5fbuffer_5fmalloc_2ecpp',['cu_temp_buffer_malloc.cpp',['../cu__temp__buffer__malloc_8cpp.html',1,'']]],
  ['cu_5ftemp_5fbuffer_5fmalloc_2eh',['cu_temp_buffer_malloc.h',['../cu__temp__buffer__malloc_8h.html',1,'']]],
  ['cu_5ftemp_5fbuffer_5fmalloc_5fmutex',['cu_temp_buffer_malloc_mutex',['../cu__temp__buffer__malloc_8cpp.html#ace0d33a8c06a48008416e8b7802f85ac',1,'cu_temp_buffer_malloc.cpp']]],
  ['cu_5futil_2ec',['cu_util.c',['../cu__util_8c.html',1,'']]],
  ['cu_5futil_2eh',['cu_util.h',['../cu__util_8h.html',1,'']]],
  ['cu_5fval',['cu_val',['../structghost__densemat.html#a8bf4b9e67031cfa773d70d7a2fa2a8a0',1,'ghost_densemat::cu_val()'],['../structghost__sparsemat.html#ac88d3bae851bd46d03863677322d1d5a',1,'ghost_sparsemat::cu_val()']]],
  ['cublas_5fcall',['CUBLAS_CALL',['../error_8h.html#ab4213cc795390a041ca64d5ce021f4e9',1,'error.h']]],
  ['cublas_5fcall_5fgoto',['CUBLAS_CALL_GOTO',['../error_8h.html#a0e99855f4afcbee24c0fb728f443abfd',1,'error.h']]],
  ['cublas_5fcall_5freturn',['CUBLAS_CALL_RETURN',['../error_8h.html#a927156e5a622d929ac315362743b89dd',1,'error.h']]],
  ['cuda',['cuda',['../structghost__tsmtspmtsm__impl.html#a413948984d2a53ee3d95fb91f9bbcb5f',1,'ghost_tsmtspmtsm_impl']]],
  ['cuda_5fcall',['CUDA_CALL',['../error_8h.html#a6d77276787336e8f00f8514a5cf73ed2',1,'error.h']]],
  ['cuda_5fcall_5fgoto',['CUDA_CALL_GOTO',['../error_8h.html#a49f9d731af09ffbce68b0c7623e3fd40',1,'error.h']]],
  ['cuda_5fcall_5freturn',['CUDA_CALL_RETURN',['../error_8h.html#a9ead33a3c2a7c7f2a5637224958acb85',1,'error.h']]],
  ['cudevice',['cudevice',['../structghost__hwconfig.html#aa90cd407455dcd406a51238571164e8e',1,'ghost_hwconfig']]],
  ['curand_5fcall',['CURAND_CALL',['../error_8h.html#a6a5affdd2ca58487a2e7485ebdb49672',1,'error.h']]],
  ['curand_5fcall_5fgoto',['CURAND_CALL_GOTO',['../error_8h.html#aa7a7fbb454edf030b1e61963f568321b',1,'error.h']]],
  ['curand_5fcall_5freturn',['CURAND_CALL_RETURN',['../error_8h.html#a9c27fa8c4a0ddeac54e265094da20403',1,'error.h']]],
  ['cusparse_5fcall',['CUSPARSE_CALL',['../error_8h.html#a1f31ebc63c9f28e075b36f4aeb350812',1,'error.h']]],
  ['cusparse_5fcall_5fgoto',['CUSPARSE_CALL_GOTO',['../error_8h.html#a84373771498654870a188883d0e14844',1,'error.h']]],
  ['cusparse_5fcall_5freturn',['CUSPARSE_CALL_RETURN',['../error_8h.html#a21da0e001c7ef10736a31c37ee4193da',1,'error.h']]],
  ['customseed',['customseed',['../rand_8c.html#ac8a0ea987b4b067e6d0627e3b0370c70',1,'rand.c']]],
  ['customsum',['CustomSum',['../structCustomSum.html',1,'']]],
  ['cv',['cv',['../structghost__sparsemat.html#ac3c7c7486c53d86dd9b5453d165069d3',1,'ghost_sparsemat']]],
  ['cz_5fghost_5fcastarray',['cz_ghost_castarray',['../bincrs_8cpp.html#a170d4a202788be5a02d3d669e441b6ac',1,'bincrs.cpp']]],
  ['code_20generation',['Code generation',['../md__home_travis_build_RRZE-HPC_GHOST_doxygen_codegeneration.html',1,'']]],
  ['context_20and_20map',['Context and map',['../md__home_travis_build_RRZE-HPC_GHOST_doxygen_context.html',1,'']]],
  ['code_20instrumentation',['Code instrumentation',['../md__home_travis_build_RRZE-HPC_GHOST_doxygen_instrumentation.html',1,'']]]
];
