var searchData=
[
  ['val',['val',['../structghost__densemat.html#ae295884a8b4d0ec32e2ac0d60dfcd243',1,'ghost_densemat::val()'],['../structghost__sparsemat__rowfunc__crs__arg.html#ab5a0f61512ba010d1ac3db25c409cd38',1,'ghost_sparsemat_rowfunc_crs_arg::val()'],['../structghost__sparsemat.html#a9dd629eb2f495ccde3e0169536df39f0',1,'ghost_sparsemat::val()']]],
  ['variance',['variance',['../structghost__sparsemat.html#a5efec9c9edbb27748b2b99af0639e37b',1,'ghost_sparsemat']]],
  ['vcols',['vcols',['../structghost__tsmm__parameters.html#ae007e950b43bb870a32fc671829eee2b',1,'ghost_tsmm_parameters::vcols()'],['../structghost__tsmttsm__parameters.html#a9fdcc8ccbd6c0c4aa4f5e24731f6d679',1,'ghost_tsmttsm_parameters::vcols()']]],
  ['vdt',['vdt',['../structghost__sellspmv__parameters.html#a657a8304a83c02b10c49c70e44890dc4',1,'ghost_sellspmv_parameters::vdt()'],['../structghost__cusellspmv__parameters.html#af414bcee6fdb7e4e0907e041a301647d',1,'ghost_cusellspmv_parameters::vdt()'],['../structghost__kacz__parameters.html#a89eb41af8a7c7980e8bbc15f0226c9e9',1,'ghost_kacz_parameters::vdt()']]],
  ['vecncols',['vecncols',['../structghost__kacz__perf__args.html#a6ab3cb1054dbe9675ff089f6b7a574a5',1,'ghost_kacz_perf_args::vecncols()'],['../structghost__spmv__perf__args.html#a7d94d3b154cc8734495189cd179072eb',1,'ghost_spmv_perf_args::vecncols()']]],
  ['version',['version',['../structghost__bincrs__header__t.html#a44a2cf4344366a5be193af2fc6768a9c',1,'ghost_bincrs_header_t']]]
];
