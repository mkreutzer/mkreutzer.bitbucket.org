var searchData=
[
  ['find_5ftransition_5fzone',['find_transition_zone',['../rcm__dissection_8cpp.html#a013f33b4647a452b4d27ebdeb15d3445',1,'find_transition_zone(ghost_sparsemat *mat, int n_threads):&#160;rcm_dissection.cpp'],['../rcm__dissection_8h.html#a013f33b4647a452b4d27ebdeb15d3445',1,'find_transition_zone(ghost_sparsemat *mat, int n_threads):&#160;rcm_dissection.cpp']]],
  ['find_5fzone_5fextrema',['find_zone_extrema',['../kacz__hybrid__split_8c.html#a7a0387acc9c4acd0c48472c29e741982',1,'find_zone_extrema(ghost_sparsemat *mat, int **extrema, ghost_lidx a, ghost_lidx b):&#160;kacz_hybrid_split.c'],['../kacz__hybrid__split_8h.html#a7a0387acc9c4acd0c48472c29e741982',1,'find_zone_extrema(ghost_sparsemat *mat, int **extrema, ghost_lidx a, ghost_lidx b):&#160;kacz_hybrid_split.c']]],
  ['fromreal',['fromReal',['../cu__complex_8h.html#a9e1135e4f2daa55a35870410a0ee416f',1,'cu_complex.h']]],
  ['fromreal_3c_20cudoublecomplex_2c_20double_20_3e',['fromReal&lt; cuDoubleComplex, double &gt;',['../cu__complex_8h.html#a4ace48ba7b39f36a8d0dda2bd1f50323',1,'cu_complex.h']]],
  ['fromreal_3c_20cufloatcomplex_2c_20float_20_3e',['fromReal&lt; cuFloatComplex, float &gt;',['../cu__complex_8h.html#a0883ee3c98db9dc30e2ba1a07ac4c697',1,'cu_complex.h']]]
];
