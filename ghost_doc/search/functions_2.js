var searchData=
[
  ['calculate_5fbw',['calculate_bw',['../sparsemat_8h.html#a35485c6fdfb7fa40f54827b13f19a4cd',1,'sparsemat.h']]],
  ['cc_5fghost_5fcastarray',['cc_ghost_castarray',['../bincrs_8cpp.html#a5d223e8fbb767fe1241a883e27e7d625',1,'bincrs.cpp']]],
  ['cd_5fghost_5fcastarray',['cd_ghost_castarray',['../bincrs_8cpp.html#a9f843f206fefbbbaa1feb4549b0b36fd',1,'bincrs.cpp']]],
  ['charfield2string',['charfield2string',['../densemat_8c.html#a6112f0d07626de48bde738756d7f2dca',1,'densemat.c']]],
  ['checker',['checker',['../kacz__hybrid__split_8c.html#a9c82e0d582c8f898cf2c1a90f0cfe3c0',1,'checker(ghost_sparsemat *mat):&#160;kacz_hybrid_split.c'],['../kacz__hybrid__split_8h.html#a9c82e0d582c8f898cf2c1a90f0cfe3c0',1,'checker(ghost_sparsemat *mat):&#160;kacz_hybrid_split.c'],['../sparsemat_8h.html#a9c82e0d582c8f898cf2c1a90f0cfe3c0',1,'checker(ghost_sparsemat *mat):&#160;kacz_hybrid_split.c']]],
  ['checker_5frcm',['checker_rcm',['../rcm__dissection_8cpp.html#ad8b685635df8324fcf8a35c05084ed78',1,'checker_rcm(ghost_sparsemat *mat):&#160;rcm_dissection.cpp'],['../rcm__dissection_8h.html#ad8b685635df8324fcf8a35c05084ed78',1,'checker_rcm(ghost_sparsemat *mat):&#160;rcm_dissection.cpp']]],
  ['conj',['conj',['../cu__complex_8h.html#a3b6f27fad013a8c1a67d0ddc9ff4b6a4',1,'cu_complex.h']]],
  ['conj_3c_20cudoublecomplex_20_3e',['conj&lt; cuDoubleComplex &gt;',['../cu__complex_8h.html#a59ce0abc0f20d9c28236741ba2de1b20',1,'cu_complex.h']]],
  ['conj_3c_20cufloatcomplex_20_3e',['conj&lt; cuFloatComplex &gt;',['../cu__complex_8h.html#a10029592a73be3d55a0aea6d8840830f',1,'cu_complex.h']]],
  ['conj_5for_5fnop',['conj_or_nop',['../namespaceghost.html#a0c5ffcacb6a285105e12124133036bd4',1,'ghost::conj_or_nop(const T &amp;a)'],['../namespaceghost.html#a050d515d5deca4a1acf254479e5cf4f6',1,'ghost::conj_or_nop(const float &amp;a)'],['../namespaceghost.html#a706c93b8e658e91c2582879950e5786a',1,'ghost::conj_or_nop(const double &amp;a)']]],
  ['cpuid',['cpuid',['../machine_8c.html#a469d2e95c1f7d083f28066725a61ee88',1,'machine.c']]],
  ['cs_5fghost_5fcastarray',['cs_ghost_castarray',['../bincrs_8cpp.html#abf30b06963bcda2abcc15a231ba5920c',1,'bincrs.cpp']]],
  ['cz_5fghost_5fcastarray',['cz_ghost_castarray',['../bincrs_8cpp.html#a170d4a202788be5a02d3d669e441b6ac',1,'bincrs.cpp']]]
];
