var searchData=
[
  ['rest',['REST',['../log_8h.html#a1ca078cc5836ad29afe7ea13c181431a',1,'log.h']]],
  ['rest_5fhelper',['REST_HELPER',['../log_8h.html#a497945f34cfaec5f20d7c4f566c7ad5c',1,'log.h']]],
  ['rest_5fhelper2',['REST_HELPER2',['../log_8h.html#ab0aff27604f6e7ac803191c7a9ca4b88',1,'log.h']]],
  ['rest_5fhelper_5fone',['REST_HELPER_ONE',['../log_8h.html#a41766055948d11e83a8f26004051d58b',1,'log.h']]],
  ['rest_5fhelper_5ftwoormore',['REST_HELPER_TWOORMORE',['../log_8h.html#ab8151b9b8bf10c35d492eb6e77ede16f',1,'log.h']]],
  ['rm_5ffuncname',['RM_FUNCNAME',['../densemat_8c.html#afc455dcb129585bbd8d367025f9721b0',1,'RM_FUNCNAME():&#160;densemat.c'],['../densemat_8cpp.html#afc455dcb129585bbd8d367025f9721b0',1,'RM_FUNCNAME():&#160;densemat.cpp']]],
  ['rotl32',['ROTL32',['../locality_8c.html#ae805660ca590323588beda4cabe00581',1,'locality.c']]],
  ['rowmajor',['ROWMAJOR',['../densemat__rm_8c.html#ac60fea3ffdeac6b995d5137ceaa108a9',1,'ROWMAJOR():&#160;densemat_rm.c'],['../densemat__rm_8cpp.html#ac60fea3ffdeac6b995d5137ceaa108a9',1,'ROWMAJOR():&#160;densemat_rm.cpp'],['../densemat__rm__averagehalo_8cpp.html#ac60fea3ffdeac6b995d5137ceaa108a9',1,'ROWMAJOR():&#160;densemat_rm_averagehalo.cpp']]]
];
