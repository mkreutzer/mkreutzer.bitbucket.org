var searchData=
[
  ['elsize',['elSize',['../structghost__densemat.html#a5ce35bda285aa77cd884f5e2f1f2934d',1,'ghost_densemat::elSize()'],['../structghost__sparsemat.html#ac5ca8415a454b39a1358db07ff2b5a89',1,'ghost_sparsemat::elSize()']]],
  ['endianess',['endianess',['../structghost__bincrs__header__t.html#ac0dad3b575a0e8881bf15cb1ead686fb',1,'ghost_bincrs_header_t']]],
  ['entsincol',['entsInCol',['../structghost__context.html#a4d4d3f108bf23db844e09473c30f9406',1,'ghost_context']]],
  ['errorhandlers',['errorhandlers',['../errorhandler_8cpp.html#ad1d02a4a9144246278ed47375f9d7eb9',1,'errorhandler.cpp']]],
  ['eta',['eta',['../structghost__spmv__opts.html#a2ae0800f39d4cd37221a948c1f8541ce',1,'ghost_spmv_opts']]]
];
