var searchData=
[
  ['idx',['idx',['../structghost__permutation__ent__t.html#aac52cdf20f908361ee684f10d6a70ce2',1,'ghost_permutation_ent_t']]],
  ['imag',['Imag',['../cu__complex_8h.html#a4c2f02dc836079838d9d6ec8e2ebc292',1,'cu_complex.h']]],
  ['imag_3c_20cudoublecomplex_2c_20double_20_3e',['Imag&lt; cuDoubleComplex, double &gt;',['../cu__complex_8h.html#abf289fe1c9537fa3ec916cd4e172dbcc',1,'cu_complex.h']]],
  ['imag_3c_20cufloatcomplex_2c_20float_20_3e',['Imag&lt; cuFloatComplex, float &gt;',['../cu__complex_8h.html#a29693b8a09cb537d0dfd6ee2c24ffd8b',1,'cu_complex.h']]],
  ['impl',['impl',['../structghost__dot__parameters.html#a6239d2e501bccebd1eb4235d904dfea0',1,'ghost_dot_parameters::impl()'],['../structghost__sellspmv__parameters.html#a681c505231424e82adbcfce929cebf01',1,'ghost_sellspmv_parameters::impl()'],['../structghost__cusellspmv__parameters.html#ae2a47baa6fa80142d27755d062d1c430',1,'ghost_cusellspmv_parameters::impl()'],['../structghost__kacz__parameters.html#af3571d070c52019bf2d1752d167a88f2',1,'ghost_kacz_parameters::impl()'],['../structghost__tsmm__parameters.html#a0824296655c84ca376c56dc868d09019',1,'ghost_tsmm_parameters::impl()'],['../structghost__tsmm__inplace__parameters.html#ae6f7574433107a7111c9fa63f230eeb4',1,'ghost_tsmm_inplace_parameters::impl()'],['../structghost__tsmttsm__parameters.html#a3c7428076128bbbda1363c632de2b767',1,'ghost_tsmttsm_parameters::impl()']]],
  ['in_5fa',['IN_A',['../structghost__compatible__vec__init.html#a2d6e0f7e4756989bfb293f4fd399d5ef',1,'ghost_compatible_vec_init']]],
  ['in_5fb',['IN_B',['../structghost__compatible__vec__init.html#ad809d6dd0f8fe707059fed56a8081be9',1,'ghost_compatible_vec_init']]],
  ['inithaloavg',['initHaloAvg',['../sparsemat_8c.html#a612a627f823065bae8f568547f1185af',1,'sparsemat.c']]],
  ['initialized',['initialized',['../structghost__kacz__opts.html#ab0150e51922e0391466a67d0909c41c8',1,'ghost_kacz_opts::initialized()'],['../structghost__carp__opts.html#ac0298670998402753712776a485d755e',1,'ghost_carp_opts::initialized()'],['../core_8c.html#ad06983e7f6e71b233ea7ff3dee1952f2',1,'initialized():&#160;core.c']]],
  ['instr_2ec',['instr.c',['../instr_8c.html',1,'']]],
  ['instr_2eh',['instr.h',['../instr_8h.html',1,'']]],
  ['instrumentation_2emd',['instrumentation.md',['../instrumentation_8md.html',1,'']]],
  ['i_2fo_20of_20vector_20and_20matrix_20data',['I/O of vector and matrix data',['../group__io.html',1,'']]],
  ['is_5faligned',['IS_ALIGNED',['../util_8h.html#afdfb6444a6b29789b5336730ca58c8e1',1,'util.h']]],
  ['ispoweroftwo',['ISPOWEROFTWO',['../util_8h.html#ac081756a39d7440ccab7bce81cb73631',1,'util.h']]]
];
