var searchData=
[
  ['labelwidth',['LABELWIDTH',['../util_8c.html#a5a7195b7b04722d0ecfd28d5b8be3940',1,'util.c']]],
  ['ld',['LD',['../structghost__task.html#ad22eec0b5e0ff87da8a1ce687788b90a',1,'ghost_task']]],
  ['ldim',['ldim',['../structghost__map.html#ad4ad081a500340e00974fef99b412a9c',1,'ghost_map']]],
  ['left1',['left1',['../structghost__compatible__mat__vec.html#ad936d2bb021fcef37e1e0193bddb13b1',1,'ghost_compatible_mat_vec']]],
  ['left2',['left2',['../structghost__compatible__mat__vec.html#a163ca80c10b88d8c9645e21c66c4ed85',1,'ghost_compatible_mat_vec']]],
  ['left3',['left3',['../structghost__compatible__mat__vec.html#a86db45a677bebb8efb5e269804cb147e',1,'ghost_compatible_mat_vec']]],
  ['left4',['left4',['../structghost__compatible__mat__vec.html#afb2b99909b2109a6b471effc8dd2e2bc',1,'ghost_compatible_mat_vec']]],
  ['loc_5fperm',['loc_perm',['../structghost__map.html#addc8785c19a26bab18eaf45e432e39a7',1,'ghost_map']]],
  ['loc_5fperm_5finv',['loc_perm_inv',['../structghost__map.html#a6150234c7cffc836706fef897f013505',1,'ghost_map']]],
  ['local_5fhostname_5fmax',['LOCAL_HOSTNAME_MAX',['../locality_8c.html#acdb24cdbfe01e7168af56fdab3dd95aa',1,'locality.c']]],
  ['localdot_5fonthefly',['LOCALDOT_ONTHEFLY',['../sell__spmv__cu__kernel_8h.html#a14d96c6552f6926bb21428605226bbb6',1,'sell_spmv_cu_kernel.h']]],
  ['locality_2ec',['locality.c',['../locality_8c.html',1,'']]],
  ['locality_2eh',['locality.h',['../locality_8h.html',1,'']]],
  ['localpart',['localPart',['../structghost__sparsemat.html#a264084d8dbccd74228caa10c0744b064',1,'ghost_sparsemat']]],
  ['location',['location',['../structghost__densemat__traits.html#a220229171cce486eb4894e3f1b5113d0',1,'ghost_densemat_traits']]],
  ['lock_5fneighbour',['LOCK_NEIGHBOUR',['../sell__kacz__bmc_8c.html#aa95df7fa62f175e4689135306c49d62f',1,'LOCK_NEIGHBOUR():&#160;sell_kacz_bmc.c'],['../sell__kacz__fallback_8cpp.html#aa95df7fa62f175e4689135306c49d62f',1,'LOCK_NEIGHBOUR():&#160;sell_kacz_fallback.cpp']]],
  ['local_20operations',['Local operations',['../group__locops.html',1,'']]],
  ['log_2eh',['log.h',['../log_8h.html',1,'']]],
  ['loop',['LOOP',['../sell__kacz__bmc_8c.html#a0bab189657cfd87176d8429fde64c93d',1,'sell_kacz_bmc.c']]],
  ['loop_5fin_5fchunk',['LOOP_IN_CHUNK',['../sell__kacz__fallback_8cpp.html#a24979eebdab26e5939cf3e6a0829bacd',1,'sell_kacz_fallback.cpp']]],
  ['loopinchunk',['LOOPINCHUNK',['../sell__kacz__mc_8c.html#a82afd5b73c020cff3e09c9cb2a4accb7',1,'sell_kacz_mc.c']]],
  ['lowerbandwidth',['lowerBandwidth',['../structghost__context.html#a5ad1ed56eb31b13eb91e6cc96a8f9664',1,'ghost_context']]],
  ['lowerperc90dists',['lowerPerc90Dists',['../sparsemat_8cpp.html#a76586b8dc9a92e98e4b302689f36ee42',1,'sparsemat.cpp']]]
];
