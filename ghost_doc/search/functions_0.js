var searchData=
[
  ['accu',['accu',['../cu__complex_8h.html#a2789308af79aa804f2cb84cdf3495d35',1,'cu_complex.h']]],
  ['accu_3c_20cudoublecomplex_20_3e',['accu&lt; cuDoubleComplex &gt;',['../cu__complex_8h.html#ae15529c347f3ea5ecd788985f4bfc5f8',1,'cu_complex.h']]],
  ['accu_3c_20cufloatcomplex_20_3e',['accu&lt; cuFloatComplex &gt;',['../cu__complex_8h.html#aba6dd293879384d431d7d8a4a3150662',1,'cu_complex.h']]],
  ['array_5fstrictly_5fascending',['array_strictly_ascending',['../densemat_8c.html#a241792de3c7fcd17af47b366aefde150',1,'array_strictly_ascending(ghost_lidx *coffs, ghost_lidx nc):&#160;densemat.c'],['../densemat_8h.html#a241792de3c7fcd17af47b366aefde150',1,'array_strictly_ascending(ghost_lidx *coffs, ghost_lidx nc):&#160;densemat.c']]],
  ['axpby',['axpby',['../cu__complex_8h.html#ac146ceae548a8cfa2d095cdfd9627ce3',1,'cu_complex.h']]],
  ['axpby_3c_20cudoublecomplex_20_3e',['axpby&lt; cuDoubleComplex &gt;',['../cu__complex_8h.html#af21d77d7989b1f40f6d7e1a814be5d93',1,'cu_complex.h']]],
  ['axpby_3c_20cufloatcomplex_20_3e',['axpby&lt; cuFloatComplex &gt;',['../cu__complex_8h.html#a231e61904ae01285e8cc00423223c9ee',1,'cu_complex.h']]],
  ['axpy',['axpy',['../cu__complex_8h.html#af8389af895c8097393d960793a7f51e7',1,'cu_complex.h']]],
  ['axpy_3c_20cudoublecomplex_2c_20cudoublecomplex_20_3e',['axpy&lt; cuDoubleComplex, cuDoubleComplex &gt;',['../cu__complex_8h.html#abaefe067bff901426a97b0a9dd2789ea',1,'cu_complex.h']]],
  ['axpy_3c_20cudoublecomplex_2c_20cufloatcomplex_20_3e',['axpy&lt; cuDoubleComplex, cuFloatComplex &gt;',['../cu__complex_8h.html#a90873ba00a9d126b6fa76da7190b9457',1,'cu_complex.h']]],
  ['axpy_3c_20cudoublecomplex_2c_20double_20_3e',['axpy&lt; cuDoubleComplex, double &gt;',['../cu__complex_8h.html#af20ed2e86cbf4aa18c498e3078236ae9',1,'cu_complex.h']]],
  ['axpy_3c_20cudoublecomplex_2c_20float_20_3e',['axpy&lt; cuDoubleComplex, float &gt;',['../cu__complex_8h.html#ae66b8d07a814398b2b73be9ad0ef445e',1,'cu_complex.h']]],
  ['axpy_3c_20cufloatcomplex_2c_20cudoublecomplex_20_3e',['axpy&lt; cuFloatComplex, cuDoubleComplex &gt;',['../cu__complex_8h.html#a77f7b9dec8fe83a34cc2c94efc66ec57',1,'cu_complex.h']]],
  ['axpy_3c_20cufloatcomplex_2c_20cufloatcomplex_20_3e',['axpy&lt; cuFloatComplex, cuFloatComplex &gt;',['../cu__complex_8h.html#ae8fed1383818bf668ee6ee781cf6d13e',1,'cu_complex.h']]],
  ['axpy_3c_20cufloatcomplex_2c_20double_20_3e',['axpy&lt; cuFloatComplex, double &gt;',['../cu__complex_8h.html#aa5058dc390c91aee88490495c93d1dde',1,'cu_complex.h']]],
  ['axpy_3c_20cufloatcomplex_2c_20float_20_3e',['axpy&lt; cuFloatComplex, float &gt;',['../cu__complex_8h.html#a86c88c61a22fdb8e2a5182b86870d535',1,'cu_complex.h']]],
  ['axpy_3c_20double_2c_20cudoublecomplex_20_3e',['axpy&lt; double, cuDoubleComplex &gt;',['../cu__complex_8h.html#ade0aebe70f10fbb8481df03f8cffbd5d',1,'cu_complex.h']]],
  ['axpy_3c_20double_2c_20cufloatcomplex_20_3e',['axpy&lt; double, cuFloatComplex &gt;',['../cu__complex_8h.html#ad48f20241205c9c27f72f4bfb47271d3',1,'cu_complex.h']]],
  ['axpy_3c_20float_2c_20cudoublecomplex_20_3e',['axpy&lt; float, cuDoubleComplex &gt;',['../cu__complex_8h.html#ad7ec1eaa817d231d775f6da0c75fa8df',1,'cu_complex.h']]],
  ['axpy_3c_20float_2c_20cufloatcomplex_20_3e',['axpy&lt; float, cuFloatComplex &gt;',['../cu__complex_8h.html#a4f2d90f1cdb0ce4ffeca33bddfa6489d',1,'cu_complex.h']]]
];
